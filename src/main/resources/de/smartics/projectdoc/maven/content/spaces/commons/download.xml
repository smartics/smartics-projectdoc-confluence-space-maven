<!--

    Copyright 2015-2024 smartics, Kronseder & Reiner GmbH

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<ac:layout>
  <ac:layout-section ac:type="single">
    <ac:layout-cell>
      <ac:structured-macro
        ac:name="projectdoc-properties-marker">
        <ac:parameter ac:name="doctype">topic</ac:parameter>
        <ac:parameter ac:name="extract-short-desc">true</ac:parameter>
        <ac:parameter ac:name="render-as">definition-list</ac:parameter>
        <ac:parameter ac:name="override">true</ac:parameter>
        <ac:rich-text-body>
          <div class="table-wrap">
            <table class="confluenceTable">
              <tbody>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.shortDescription"/></th>
                  <td class="confluenceTd"><at:i18n at:key="projectdoc.maven.document.download.shortDescription"/></td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.name"/></th>
                  <td class="confluenceTd"><at:i18n at:key="projectdoc.maven.document.download.name"/></td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.parent"/></th>
                  <td class="confluenceTd">
                    <ac:structured-macro ac:name="projectdoc-transclusion-parent-property">
                      <ac:parameter ac:name="property-name"><at:i18n at:key="projectdoc.doctype.common.name"/></ac:parameter>
                    </ac:structured-macro>
                  </td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.audience"/></th>
                  <td class="confluenceTd">
                    <ac:structured-macro ac:name="projectdoc-name-list">
                      <ac:parameter ac:name="doctype">role</ac:parameter>
                      <ac:parameter ac:name="property"><at:i18n at:key="projectdoc.doctype.common.audience"/></ac:parameter>
                      <ac:parameter ac:name="names"><at:i18n at:key="projectdoc.maven.enums.roles.user"/></ac:parameter>
                      <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
                    </ac:structured-macro></td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.topic.level-of-experience-required"/></th>
                  <td class="confluenceTd">
                    <ac:structured-macro ac:name="projectdoc-name-list">
                      <ac:parameter ac:name="doctype">experience-level</ac:parameter>
                      <ac:parameter ac:name="property"><at:i18n at:key="projectdoc.doctype.topic.level-of-experience-required"/></ac:parameter>
                      <ac:parameter ac:name="names"><at:i18n at:key="projectdoc.maven.enums.level-of-experience.novice"/></ac:parameter>
                      <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
                    </ac:structured-macro>
                  </td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.topic.expected-duration"/></th>
                  <td class="confluenceTd"><ac:placeholder><at:i18n at:key="projectdoc.doctype.topic.expected-duration.placeholder"/></ac:placeholder></td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.subject"/></th>
                  <td class="confluenceTd"><ac:structured-macro ac:name="projectdoc-name-list">
                      <ac:parameter ac:name="doctype">subject</ac:parameter>
                      <ac:parameter ac:name="property"><at:i18n at:key="projectdoc.doctype.common.subject"/></ac:parameter>
                    </ac:structured-macro></td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.categories"/></th>
                  <td class="confluenceTd"><ac:structured-macro ac:name="projectdoc-name-list">
                      <ac:parameter ac:name="doctype">category</ac:parameter>
                      <ac:parameter ac:name="property"><at:i18n at:key="projectdoc.doctype.common.categories"/></ac:parameter>
                    </ac:structured-macro></td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.tags"/></th>
                  <td class="confluenceTd"><ac:structured-macro ac:name="projectdoc-tag-list-macro">
                      <ac:parameter ac:name="property"><at:i18n at:key="projectdoc.doctype.common.tags"/></ac:parameter>
                      <ac:parameter ac:name="names"><at:i18n at:key="projectdoc.maven.document.tag.technical"/></ac:parameter>
                    </ac:structured-macro></td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.iteration"/></th>
                  <td class="confluenceTd"><ac:structured-macro ac:name="projectdoc-iteration">
                      <ac:parameter ac:name="value">finished</ac:parameter>
                    </ac:structured-macro>
                  </td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.topic.type"/></th>
                  <td class="confluenceTd">
                    <ac:structured-macro ac:name="projectdoc-name-list">
                      <ac:parameter ac:name="doctype">topic-type</ac:parameter>
                      <ac:parameter ac:name="property"><at:i18n at:key="projectdoc.doctype.topic.type"/></ac:parameter>
                      <ac:parameter ac:name="names"><at:i18n at:key="projectdoc.maven.document.topic-type.howto"/></ac:parameter>
                      <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
                    </ac:structured-macro>
                  </td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.sortKey"/></th>
                  <td class="confluenceTd"><ac:placeholder><at:i18n at:key="projectdoc.doctype.common.sortKey.placeholder"/></ac:placeholder></td>
                  <td class="confluenceTd">hide</td>
                </tr>
              </tbody>
            </table>
          </div>
        </ac:rich-text-body>
      </ac:structured-macro>
    </ac:layout-cell>
  </ac:layout-section>

  <ac:layout-section ac:type="single">
    <ac:layout-cell>
      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.common.description"/></ac:parameter>
        <ac:rich-text-body>
          <p>
            <at:i18n at:key="projectdoc.doctype.common.description.p1.1"/>
            <ac:structured-macro
              ac:macro-id="adaed840-1807-4e6d-b0f3-68f56b54a11c"
              ac:name="projectdoc-information-system-generic-macro"
              ac:schema-version="1">
              <ac:parameter ac:name="system-id">nexus</ac:parameter>
              <ac:parameter ac:name="label"><at:i18n at:key="projectdoc.maven.label.artifactRepository"/></ac:parameter>
            </ac:structured-macro><at:i18n at:key="projectdoc.doctype.common.description.p1.2"/>
          </p>
          <p>
            <at:i18n at:key="projectdoc.doctype.common.description.p2.1"/>
            <a href="https://docs.oracle.com/javase/tutorial/deployment/jar/index.html">JAR</a>
            <at:i18n at:key="projectdoc.doctype.common.description.p2.2"/>
          </p>
        </ac:rich-text-body>
      </ac:structured-macro>

      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.topic.prerequisites"/></ac:parameter>
        <ac:rich-text-body>
          <ac:placeholder><at:i18n at:key="projectdoc.doctype.topic.prerequisites.placeholder"/></ac:placeholder>
        </ac:rich-text-body>
      </ac:structured-macro>

      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.topic.abstract"/></ac:parameter>
        <ac:rich-text-body>
          <ac:placeholder><at:i18n at:key="projectdoc.doctype.topic.abstract.placeholder"/></ac:placeholder>
        </ac:rich-text-body>
      </ac:structured-macro>

      <ac:structured-macro
        ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.maven.document.download.section.maven.header"/></ac:parameter>
        <ac:rich-text-body>
          <p>
            <at:i18n at:key="projectdoc.maven.document.download.section.maven.p1.1"/>
            <a href="http://maven.apache.org/">Maven</a>
            <at:i18n at:key="projectdoc.maven.document.download.section.maven.p1.2"/>
            <a href="https://www.oracle.com/java/">Java</a>
            <at:i18n at:key="projectdoc.maven.document.download.section.maven.p1.3"/>
          </p>
          <ac:structured-macro
            ac:name="projectdoc-section">
            <ac:parameter ac:name="level">2</ac:parameter>
            <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.maven.document.download.section.maven.dependency.header"/></ac:parameter>
            <ac:rich-text-body>
              <p>
                <at:i18n at:key="projectdoc.maven.document.download.section.maven.dependency.p1"/>
                <ac:structured-macro
                  ac:name="projectdoc-information-system-nexus-macro">
                  <ac:parameter ac:name="code-title">?gav</ac:parameter>
                  <ac:parameter ac:name="render-link">false</ac:parameter>
                  <ac:parameter ac:name="render-xml-snippet">true</ac:parameter>
                  <ac:parameter ac:name="is-library">false</ac:parameter>
                  <ac:parameter ac:name="atlassian-macro-output-type">INLINE</ac:parameter>
                  <ac:plain-text-body><![CDATA[ ]]></ac:plain-text-body>
                </ac:structured-macro>
              </p>
            </ac:rich-text-body>
          </ac:structured-macro>
          <ac:structured-macro
            ac:name="projectdoc-content-marker">
            <ac:parameter ac:name="required-space-properties">info.server.repo.group</ac:parameter>
            <ac:rich-text-body>
              <ac:structured-macro
                ac:name="projectdoc-section">
                <ac:parameter ac:name="level">2</ac:parameter>
                <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.maven.document.download.section.maven.corporationRepository.header"/></ac:parameter>
                <ac:rich-text-body>
                  <p>
                    <at:i18n at:key="projectdoc.maven.document.download.section.maven.corporationRepository.p1.1"/>
                    <strong><at:i18n at:key="projectdoc.maven.document.download.section.maven.corporationRepository.p1.2"/></strong>
                    <at:i18n at:key="projectdoc.maven.document.download.section.maven.corporationRepository.p1.3"/>
                    <a
                      class="external-link"
                      href="http://repository.sonatype.org/index.html#welcome"
                      rel="nofollow">Maven Central Repository</a><at:i18n at:key="projectdoc.maven.document.download.section.maven.corporationRepository.p1.4"/>
                    <ac:structured-macro
                      ac:name="projectdoc-information-system-generic-macro">
                      <ac:parameter ac:name="system-id">nexus</ac:parameter>
                      <ac:parameter ac:name="label"><at:i18n at:key="projectdoc.maven.document.download.section.maven.corporationRepository.p1.5"/></ac:parameter>
                    </ac:structured-macro><at:i18n at:key="projectdoc.maven.document.download.section.maven.corporationRepository.p1.6"/>
                  </p>
                  <p>
                    <at:i18n at:key="projectdoc.maven.document.download.section.maven.corporationRepository.p2.1"/>
                    <a href="http://maven.apache.org/">Maven</a>
                    <at:i18n at:key="projectdoc.maven.document.download.section.maven.corporationRepository.p2.2"/>
                  </p>
                  <p>
                    <at:i18n at:key="projectdoc.maven.document.download.section.maven.corporationRepository.p3.1"/>
                    <code><a href="https://maven.apache.org/settings.html">settings.xml</a></code><at:i18n at:key="projectdoc.maven.document.download.section.maven.corporationRepository.p3.2"/>
                  </p>
                  <ac:structured-macro
                    ac:name="projectdoc-code-block-placeholder-macro">
                    <ac:parameter ac:name="code-language">HTML and XML</ac:parameter>
                    <ac:plain-text-body><![CDATA[<profile>
  <id>${corporationId}-repositories</id>
  <repositories>
    <repository>
      <id>${corporationId}</id>
      <url>${info.server.repo.group}</url>
      <releases>
        <enabled>true</enabled>
      </releases>
      <snapshots>
        <enabled>false</enabled>
      </snapshots>
    </repository>
  </repositories>
  <pluginRepositories>
    <pluginRepository>
      <id>${corporationId}</id>
      <url>${info.server.repo.group}</url>
      <releases>
        <enabled>true</enabled>
      </releases>
      <snapshots>
        <enabled>false</enabled>
      </snapshots>
    </pluginRepository>
  </pluginRepositories>
</profile>]]></ac:plain-text-body>
                  </ac:structured-macro>
                  <p>
                    <at:i18n at:key="projectdoc.maven.document.download.section.maven.corporationRepository.p4.1"/>
                    <a
                      href="https://maven.apache.org/pom.html"><at:i18n at:key="projectdoc.maven.document.download.section.maven.corporationRepository.p4.2"/></a><at:i18n at:key="projectdoc.maven.document.download.section.maven.corporationRepository.p4.3"/>
                  </p>
                </ac:rich-text-body>
              </ac:structured-macro>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro
        ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.maven.document.download.section.download.header"/></ac:parameter>
        <ac:rich-text-body>
          <p><at:i18n at:key="projectdoc.maven.document.download.section.download.p1"/></p>
          <ac:structured-macro ac:name="panel">
            <ac:rich-text-body>
              <p>
                <ac:structured-macro
                  ac:name="projectdoc-information-system-nexus-macro">
                  <ac:parameter ac:name="code-title">?gav</ac:parameter>
                  <ac:parameter ac:name="label">?gav</ac:parameter>
                  <ac:parameter ac:name="atlassian-macro-output-type">INLINE</ac:parameter>
                  <ac:plain-text-body><![CDATA[ ]]></ac:plain-text-body>
                </ac:structured-macro>
              </p>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>

      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.topic.children"/></ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">topic</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/></ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/>, <at:i18n at:key="projectdoc.doctype.common.name"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>

      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.common.notes"/></ac:parameter>
        <ac:parameter ac:name="required-permissions">write-access</ac:parameter>
        <ac:rich-text-body>
          <ac:placeholder><at:i18n at:key="projectdoc.doctype.common.notes.placeholder"/></ac:placeholder>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.common.references"/></ac:parameter>
        <ac:rich-text-body>
          <ac:placeholder><at:i18n at:key="projectdoc.doctype.common.references.placeholder"/></ac:placeholder>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.common.resources"/></ac:parameter>
        <ac:rich-text-body>
          <ac:placeholder><at:i18n at:key="projectdoc.doctype.common.resources.placeholder"/></ac:placeholder>
        </ac:rich-text-body>
      </ac:structured-macro>
    </ac:layout-cell>
  </ac:layout-section>
</ac:layout>
