<!--

    Copyright 2015-2024 smartics, Kronseder & Reiner GmbH

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<ac:layout>
  <ac:layout-section ac:type="single">
    <ac:layout-cell>
      <ac:structured-macro ac:name="projectdoc-properties-marker">
        <ac:parameter ac:name="doctype">project</ac:parameter>
        <ac:parameter ac:name="extract-short-desc">true</ac:parameter>
        <ac:parameter ac:name="hide">true</ac:parameter>
        <ac:parameter ac:name="render-as">definition-list</ac:parameter>
        <ac:parameter ac:name="override">true</ac:parameter>
        <ac:rich-text-body>
          <div class="table-wrap">
            <table>
              <tbody>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.shortDescription"/></th>
                  <td class="confluenceTd"><ac:structured-macro ac:name="projectdoc-display-space-attribute-macro">
                    <ac:parameter ac:name="attribute">description</ac:parameter>
                  </ac:structured-macro></td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.name"/></th>
                  <td class="confluenceTd"><at:var at:name="name" /><at:var at:name="projectdoc_doctype_common_name" /></td>
                  <td class="confluenceTd">hide</td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.tags"/></th>
                  <td class="confluenceTd">
                    <ac:structured-macro ac:name="projectdoc-tag-list-macro">
                      <ac:parameter ac:name="names">maven-plugin</ac:parameter>
                    </ac:structured-macro>
                  </td>
                  <td class="confluenceTd">hide</td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.spaceTags"/></th>
                  <td class="confluenceTd"><ac:structured-macro ac:name="projectdoc-display-space-attribute-macro">
                    <ac:parameter ac:name="attribute">labels</ac:parameter></ac:structured-macro></td>
                  <td class="confluenceTd">hide</td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.sortKey"/></th>
                  <td class="confluenceTd"><ac:placeholder><at:i18n at:key="projectdoc.doctype.common.sortKey.placeholder"/></ac:placeholder></td>
                  <td class="confluenceTd">hide</td>
                </tr>
                <tr>
                  <th class="confluenceTh">documentation-json-uri</th>
                  <td class="confluenceTd">https://www.smartics.eu/confluence/download/attachments/12156954/docmap.json?api=v2</td>
                  <td class="confluenceTd">hide</td>
                </tr>
                <tr>
                  <th>space-properties-extension-pattern</th>
                  <td><at:var at:name="projectdoc.extension.maven.doctype.version.label.version-neutral-title" /></td>
                  <td>hide</td>
                </tr>
                <tr>
                  <th>server-root-based</th>
                  <td>false</td>
                  <td>hide</td>
                </tr>
                <tr>
                  <th>projectdoc.doctype.version.home</th>
                  <td><at:i18n at:key="projectdoc.content.version.index.all.title"/></td>
                  <td>hide</td>
                </tr>
              </tbody>
            </table>
          </div>
        </ac:rich-text-body>
      </ac:structured-macro>
    </ac:layout-cell>
  </ac:layout-section>
  <ac:layout-section ac:type="single">
    <ac:layout-cell>
      <ac:structured-macro ac:name="projectdoc-content-marker">
        <ac:parameter ac:name="required-space-properties">!project.version</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="panel">
            <ac:parameter ac:name="borderStyle">dashed</ac:parameter>
            <ac:parameter ac:name="borderWidth">3</ac:parameter>
            <ac:parameter ac:name="bgColor">#FFCC66</ac:parameter>
            <ac:parameter ac:name="borderColor">darkred</ac:parameter>
            <ac:rich-text-body>
              <p style="text-align: center;">
                <at:i18n at:key="projectdoc.content.message.pleaseReload" />
              </p>
              <ac:structured-macro ac:name="projectdoc-creation-tracker-macro"/>
              <pre class="code"><at:var at:name="projectdoc.context.error.message" at:rawxhtml="true"/></pre>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro ac:name="panel">
        <ac:parameter ac:name="borderStyle">none</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="livesearch">
            <ac:parameter ac:name="placeholder"><at:i18n at:key="projectdoc.home.label.search"/></ac:parameter>
            <ac:parameter ac:name="spaceKey"><at:var at:name="spaceKeyElement" at:rawxhtml="true"/></ac:parameter>
            <ac:parameter ac:name="size">large</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>
    </ac:layout-cell>
  </ac:layout-section>
  <ac:layout-section ac:type="two_left_sidebar">
    <ac:layout-cell>
      <ac:structured-macro ac:name="panel">
        <ac:parameter ac:name="bgColor">#FFFFF0</ac:parameter>
        <ac:parameter ac:name="borderStyle">none</ac:parameter>
        <ac:rich-text-body>
          <h3>
            <at:i18n at:key="projectdoc.home.section.label.overview" />
          </h3>
          <ul>
            <li>
              <ac:structured-macro ac:name="projectdoc-link-wiki">
                <ac:parameter ac:name="label"><at:i18n at:key="projectdoc.home.section.label.introduction" /></ac:parameter>
                <ac:parameter ac:name="page">/</ac:parameter>
              </ac:structured-macro>
            </li>
            <li>
              <ac:structured-macro ac:name="projectdoc-link-wiki">
                <ac:parameter ac:name="page"><at:i18n at:key="projectdoc.maven.report.pluginDescriptor.title" /> (<at:var at:name="projectdoc.extension.maven.doctype.version.label.version-neutral-title" />)</ac:parameter>
                <ac:parameter ac:name="label"><at:i18n at:key="projectdoc.home.label.goals" /></ac:parameter>
              </ac:structured-macro>
            </li>
<!--             <li>
              <ac:structured-macro ac:name="projectdoc-information-system-generic-macro">
                <ac:parameter ac:name="system-type">site</ac:parameter>
                <ac:parameter ac:name="file">plugin-info.html</ac:parameter>
                <ac:parameter ac:name="label"><at:i18n at:key="projectdoc.home.label.goals" /></ac:parameter>
                <ac:parameter ac:name="check">true</ac:parameter>
              </ac:structured-macro>
            </li>
 -->
            <li>
              <ac:structured-macro ac:name="projectdoc-link-wiki">
                <ac:parameter ac:name="page"><at:i18n at:key="projectdoc.home.label.usage" /></ac:parameter>
                <ac:parameter ac:name="blueprint-module-complete-key">de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:projectdoc-blueprint-doctype-topic</ac:parameter>
              </ac:structured-macro>
            </li>
            <li>
              <ac:structured-macro ac:name="projectdoc-link-wiki">
                <ac:parameter ac:name="page"><at:i18n at:key="projectdoc.home.label.download" /></ac:parameter>
              </ac:structured-macro>
            </li>
<!--             <li>
              <ac:structured-macro ac:name="projectdoc-information-system-generic-macro">
                <ac:parameter ac:name="system-id">javadoc</ac:parameter>
                <ac:parameter ac:name="check">true</ac:parameter>
                <ac:parameter ac:name="label"><at:i18n at:key="projectdoc.home.label.api-documentation" /></ac:parameter>
              </ac:structured-macro>
            </li>
 -->
          </ul>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro ac:name="panel">
        <ac:parameter ac:name="bgColor">#FFFFF0</ac:parameter>
        <ac:parameter ac:name="borderStyle">none</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-section">
            <ac:parameter ac:name="level">3</ac:parameter>
            <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.home.label.documentation" /></ac:parameter>
            <ac:rich-text-body>
              <p>
                <ac:structured-macro ac:name="projectdoc-display-list">
                  <ac:parameter ac:name="doctype">topic</ac:parameter>
                  <ac:parameter ac:name="where">$&lt;<at:i18n at:key="projectdoc.doctype.common.tags" />&gt; = [<at:i18n at:key="projectdoc.doctype.common.tags.main" />]</ac:parameter>
                  <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
                </ac:structured-macro>
              </p>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro ac:name="panel">
        <ac:parameter ac:name="bgColor">#FFFFF0</ac:parameter>
        <ac:parameter ac:name="borderStyle">none</ac:parameter>
        <ac:rich-text-body>
          <h3>
            <at:i18n at:key="projectdoc.home.label.references" />
          </h3>
          <ac:structured-macro ac:name="projectdoc-content-marker">
            <ac:parameter ac:name="wrapping-element">ul</ac:parameter>
            <ac:rich-text-body>
              <ac:structured-macro
                ac:name="projectdoc-content-marker">
                <ac:parameter ac:name="required-space-properties">project.blog.documented</ac:parameter>
                <ac:parameter ac:name="wrapping-element">li</ac:parameter>
                <ac:rich-text-body>
                  <at:i18n at:key="projectdoc.home.label.project-blog" />
                </ac:rich-text-body>
              </ac:structured-macro>
              <ac:structured-macro
                ac:name="projectdoc-content-marker">
                <ac:parameter ac:name="required-space-properties">project.url</ac:parameter>
                <ac:rich-text-body>
                  <ac:structured-macro ac:name="projectdoc-content-marker">
                    <ac:parameter ac:name="wrapping-element">li</ac:parameter>
                    <ac:rich-text-body>
                      <ac:structured-macro ac:name="projectdoc-information-system-generic-macro">
                        <ac:parameter ac:name="system-type">site</ac:parameter>
                        <ac:parameter ac:name="file">index.html</ac:parameter>
                        <ac:parameter ac:name="label"><at:i18n at:key="projectdoc.home.label.report-site" /></ac:parameter>
                        <ac:parameter ac:name="check">true</ac:parameter>
                      </ac:structured-macro>
                    </ac:rich-text-body>
                  </ac:structured-macro>
                  <ac:structured-macro ac:name="projectdoc-content-marker">
                    <ac:parameter ac:name="wrapping-element">li</ac:parameter>
                    <ac:rich-text-body>
                      <ac:structured-macro ac:name="projectdoc-information-system-generic-macro">
                        <ac:parameter ac:name="system-type">site</ac:parameter>
                        <ac:parameter ac:name="file">project-info.html</ac:parameter>
                        <ac:parameter ac:name="label"><at:i18n at:key="projectdoc.home.label.project-information" /></ac:parameter>
                        <ac:parameter ac:name="check">true</ac:parameter>
                      </ac:structured-macro>
                      <ac:structured-macro ac:name="projectdoc-content-marker">
                        <ac:parameter ac:name="wrapping-element">ul</ac:parameter>
                        <ac:rich-text-body>
                          <ac:structured-macro ac:name="projectdoc-content-marker">
                            <ac:parameter ac:name="required-space-properties">project.issueManagement.url</ac:parameter>
                            <ac:parameter ac:name="wrapping-element">li</ac:parameter>
                            <ac:rich-text-body>
                              <ac:structured-macro ac:name="projectdoc-information-system-generic-macro">
                                <ac:parameter ac:name="system-type">site</ac:parameter>
                                <ac:parameter ac:name="file">issue-tracking.html</ac:parameter>
                                <ac:parameter ac:name="label"><at:i18n at:key="projectdoc.home.label.issue-tracking" /></ac:parameter>
                                <ac:parameter ac:name="check">true</ac:parameter>
                              </ac:structured-macro>
                            </ac:rich-text-body>
                          </ac:structured-macro>
                          <ac:structured-macro
                            ac:name="projectdoc-content-marker">
                            <ac:parameter ac:name="required-space-properties">project.scm.url</ac:parameter>
                            <ac:parameter ac:name="wrapping-element">li</ac:parameter>
                            <ac:rich-text-body>
                              <ac:structured-macro ac:name="projectdoc-information-system-generic-macro">
                                <ac:parameter ac:name="system-type">site</ac:parameter>
                                <ac:parameter ac:name="file">source-repository.html</ac:parameter>
                                <ac:parameter ac:name="label"><at:i18n at:key="projectdoc.home.label.source-repository" /></ac:parameter>
                                <ac:parameter ac:name="check">true</ac:parameter>
                              </ac:structured-macro>
                            </ac:rich-text-body>
                          </ac:structured-macro>
                        </ac:rich-text-body>
                      </ac:structured-macro>
                    </ac:rich-text-body>
                  </ac:structured-macro>
                </ac:rich-text-body>
              </ac:structured-macro>

              <ac:structured-macro ac:name="projectdoc-content-marker">
                <ac:parameter ac:name="wrapping-element">li</ac:parameter>
                <ac:rich-text-body>
                  <at:i18n at:key="projectdoc.home.label.version-management" />
                  <ac:structured-macro
                    ac:name="projectdoc-content-marker">
                    <ac:parameter ac:name="required-space-properties">project.issueManagement.url</ac:parameter>
                    <ac:rich-text-body>
                      <ac:structured-macro
                        ac:name="projectdoc-content-marker">
                        <ac:parameter ac:name="required-space-properties">project.issueManagement.type.jira</ac:parameter>
                        <ac:rich-text-body>
                          <ul>
                            <li>
                              <ac:structured-macro ac:name="projectdoc-link-external">
                                <ac:parameter ac:name="title">projectdoc.home.label.release-notes</ac:parameter>
                                <ac:parameter ac:name="base-url">project.issueManagement.url</ac:parameter>
                                <ac:parameter ac:name="page">project.issueManagement.type.jira.version</ac:parameter>
                                <ac:rich-text-body>
                                  <at:i18n at:key="projectdoc.home.label.release-notes" />
                                </ac:rich-text-body>
                              </ac:structured-macro>
                            </li>
                            <li>
                              <ac:structured-macro ac:name="projectdoc-link-external">
                                <ac:parameter ac:name="title">projectdoc.home.label.road-map</ac:parameter>
                                <ac:parameter ac:name="base-url">project.issueManagement.url</ac:parameter>
                                <ac:parameter ac:name="page">?selectedItem=com.atlassian.jira.jira-projects-plugin:release-page</ac:parameter>
                                <ac:rich-text-body>
                                  <at:i18n at:key="projectdoc.home.label.road-map" />
                                </ac:rich-text-body>
                              </ac:structured-macro>
                            </li>
                            <li>
                              <ac:structured-macro ac:name="projectdoc-link-external">
                                <ac:parameter ac:name="title">projectdoc.home.label.change-log</ac:parameter>
                                <ac:parameter ac:name="base-url">project.issueManagement.url</ac:parameter>
                                <ac:parameter ac:name="page">?selectedItem=com.atlassian.jira.jira-projects-plugin:release-page&amp;status=released</ac:parameter>
                                <ac:rich-text-body>
                                  <at:i18n at:key="projectdoc.home.label.change-log" />
                                </ac:rich-text-body>
                              </ac:structured-macro>
                            </li>
                          </ul>
                        </ac:rich-text-body>
                      </ac:structured-macro>
                      <ac:structured-macro
                        ac:name="projectdoc-content-marker">
                        <ac:parameter ac:name="required-space-properties">!project.issueManagement.type.jira</ac:parameter>
                        <ac:rich-text-body>
                          <ul>
                            <li><at:i18n at:key="projectdoc.home.label.release-notes" /></li>
                            <li><at:i18n at:key="projectdoc.home.label.road-map" /></li>
                            <li><at:i18n at:key="projectdoc.home.label.change-log" /></li>
                          </ul>
                        </ac:rich-text-body>
                      </ac:structured-macro>
                    </ac:rich-text-body>
                  </ac:structured-macro>
                </ac:rich-text-body>
              </ac:structured-macro>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro ac:name="panel">
        <ac:parameter ac:name="bgColor">#FFFFF0</ac:parameter>
        <ac:parameter ac:name="borderStyle">none</ac:parameter>
        <ac:rich-text-body>
          <h3>
            <at:i18n at:key="projectdoc.home.label.legal-stuff" />
          </h3>
          <ac:structured-macro ac:name="projectdoc-content-marker">
            <ac:parameter ac:name="wrapping-element">ul</ac:parameter>
            <ac:rich-text-body>
              <ac:structured-macro
                ac:name="projectdoc-content-marker">
                <ac:parameter ac:name="required-space-properties">project.licenses.license.1.name</ac:parameter>
                <ac:parameter ac:name="wrapping-element">li</ac:parameter>
                <ac:rich-text-body>
                  <ac:structured-macro ac:name="projectdoc-information-system-generic-macro">
                    <ac:parameter ac:name="system-type">site</ac:parameter>
                    <ac:parameter ac:name="file">license.html</ac:parameter>
                    <ac:parameter ac:name="label"><at:i18n at:key="projectdoc.home.label.license" /></ac:parameter>
                    <ac:parameter ac:name="check">true</ac:parameter>
                  </ac:structured-macro>
                </ac:rich-text-body>
              </ac:structured-macro>
              <ac:structured-macro
                ac:name="projectdoc-content-marker">
                <ac:parameter ac:name="required-space-properties">project.credits.documented</ac:parameter>
                <ac:parameter ac:name="wrapping-element">li</ac:parameter>
                <ac:rich-text-body>
                  <ac:structured-macro ac:name="projectdoc-information-system-generic-macro">
                    <ac:parameter ac:name="system-type">site</ac:parameter>
                    <ac:parameter ac:name="file">credits.html</ac:parameter>
                    <ac:parameter ac:name="label"><at:i18n at:key="projectdoc.home.label.credits" /></ac:parameter>
                    <ac:parameter ac:name="check">true</ac:parameter>
                  </ac:structured-macro>
                </ac:rich-text-body>
              </ac:structured-macro>
              <ac:structured-macro
                ac:name="projectdoc-content-marker">
                <ac:parameter ac:name="required-space-properties">project.imprint.documented</ac:parameter>
                <ac:parameter ac:name="wrapping-element">li</ac:parameter>
                <ac:rich-text-body>
                  <ac:link>
                    <ri:page
                      ri:content-title="Imprint"
                      ri:space-key="HOMESPACE" />
                  </ac:link>
                </ac:rich-text-body>
              </ac:structured-macro>
              <ac:structured-macro
                ac:name="projectdoc-content-marker">
                <ac:parameter ac:name="required-space-properties">project.team.documented</ac:parameter>
                <ac:parameter ac:name="wrapping-element">li</ac:parameter>
                <ac:rich-text-body>
                  <ac:structured-macro ac:name="projectdoc-information-system-generic-macro">
                    <ac:parameter ac:name="system-type">site</ac:parameter>
                    <ac:parameter ac:name="file">team-list.html</ac:parameter>
                    <ac:parameter ac:name="label"><at:i18n at:key="projectdoc.home.label.team" /></ac:parameter>
                    <ac:parameter ac:name="check">true</ac:parameter>
                  </ac:structured-macro>
                </ac:rich-text-body>
              </ac:structured-macro>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro ac:name="projectdoc-hide-from-anonymous-user-macro">
        <ac:parameter ac:name="atlassian-macro-output-type">BLOCK</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="panel">
            <ac:parameter ac:name="bgColor">#EECC22</ac:parameter>
            <ac:parameter ac:name="borderStyle">none</ac:parameter>
            <ac:rich-text-body>
              <h3>
                <at:i18n at:key="projectdoc.home.label.content-management" />
              </h3>
              <ul>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page"><at:i18n at:key="projectdoc.doctype.content-management-dashboard-template.name" /></ac:parameter>
                    <ac:parameter ac:name="label"><at:i18n at:key="projectdoc.doctype.content-management-dashboard-template.name" /></ac:parameter>
                  </ac:structured-macro>
                </li>
              </ul>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>
    </ac:layout-cell>
    <ac:layout-cell>
      <p style="text-align: right;"><at:i18n at:key="projectdoc.home.label.version" />&#0032;<ac:structured-macro ac:name="projectdoc-space-property-display-macro"><ac:parameter ac:name="property-name">project.version</ac:parameter></ac:structured-macro>
      </p>
      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="level">1</ac:parameter>
        <ac:parameter ac:name="css">projectdoc-top-section</ac:parameter>
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.home.section.label.introduction" /></ac:parameter>
        <ac:rich-text-body>
          <p>
            <at:i18n at:key="projectdoc.maven.document.home.section.api.p1.1" />
            <a href="https://maven.apache.org/">Maven</a><at:i18n at:key="projectdoc.maven.document.home.section.api.p1.2" />
          </p>
        </ac:rich-text-body>
      </ac:structured-macro>
    </ac:layout-cell>
  </ac:layout-section>
</ac:layout>

