/*
 * Copyright 2015-2024 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
//Confluence.Blueprint
//.setWizard(
//  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-customer-development:create-doctype-template-java-NEW_DOCTYPE',
//  function(wizard) {
//      wizard.on('submit.page1Id', function(e, state) {
//        var name = state.pageData["projectdoc.doctype.common.name"];
//        if (!name) {
//            alert('Please provide a name for this document.');
//            return false;
//        }
//
//        var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
//        if (!shortDescription){
//            alert('Please provide a short description for the document.');
//            return false;
//        }
//    });
//  });
