/*
 * Copyright 2015-2024 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.maven.space;

import de.smartics.projectdoc.atlassian.confluence.tools.space.AbstractSpaceLabelListener;

import com.atlassian.confluence.labels.SpaceLabelManager;
import com.atlassian.confluence.plugins.createcontent.api.events.SpaceBlueprintCreateEvent;
import com.atlassian.event.api.EventListener;

/**
 * Listens to space creations and adds space labels.
 */
public class SpaceLabelListener extends AbstractSpaceLabelListener {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private static final String MODULE_KEY =
      "de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-maven:projectdoc-space-blueprint-maven-development-plugin";

  private static final String LABEL = "maven";

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public SpaceLabelListener(final SpaceLabelManager spaceLabelManager) {
    super(spaceLabelManager);
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  @Override
  protected String getModuleKey() {
    return MODULE_KEY;
  }

  @Override
  protected String getLabel() {
    return LABEL;
  }

  // --- business -------------------------------------------------------------

  /**
   * Adjusts newly created spaces.
   *
   * @param event the space event to apply adjustments to.
   */
  @EventListener
  public void onSpaceBlueprintCreate(final SpaceBlueprintCreateEvent event) {
    super.onSpaceBlueprintCreate(event);
  }

  // --- object basics --------------------------------------------------------

}
